package com.cyf.demo.demo;

import com.cyf.demo.demo.anno.Person;
import com.cyf.demo.demo.bean.MyCalculator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;

/**
 * @author cyf
 * @Description: d
 * @date 2022/8/10
 */

public class demot {

	public static void main(String[] args) throws Exception {
		ApplicationContext ac = new ClassPathXmlApplicationContext("aaa.xml");
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
	}

	public static void ma1in(String[] args) throws NoSuchMethodException {
		ApplicationContext ac = new ClassPathXmlApplicationContext("aaa.xml");
		MyCalculator bean = ac.getBean(MyCalculator.class);
		bean.add(1, 1);
		System.out.println();
	}
	public static void mai1n(String[] args) throws NoSuchMethodException {
		ApplicationContext ac = new ClassPathXmlApplicationContext("aaa.xml");
		MyCalculator bean = ac.getBean(MyCalculator.class);
		// CglibAopProxy 内部类 DynamicAdvisedInterceptor#intercept
		bean.add(1, 1);
		System.out.println();
	}

	private void test1(){
		int[] array = {1,2,3,4,5,54,12,35,123};
		// 需要获取最大值
		int maxNum = getMostNum(array);
		// ---- 后面还有代码
	}
	private void test2(){
		int[] array = {1,2,3,4,5,231,12231,1335,123};
		// 需要获取最大值
		int maxNum = getMostNum(array);
		// ---- 后面还有代码
	}
	// 抽取方法
	private int  getMostNum(int[] array){
		int result = Integer.MIN_VALUE;
		for (int i : array) {
			result = Math.max(result,i);
		}
		return result;
	}


}