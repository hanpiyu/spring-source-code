package com.cyf.demo.demo.bean;

import com.cyf.demo.demo.anno.Person;
import jakarta.annotation.Resource;
import jakarta.annotation.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author cyf
 * @Time 2023-03-29 01:48
 * @Description
 */

public class Student {


	private Person person;

	private String name;

	public Student(String name) {

	}

	public Student() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Person person() {
		return person;
	}

	public Student setPerson(Person person) {
		this.person = person;
		return this;
	}
}
