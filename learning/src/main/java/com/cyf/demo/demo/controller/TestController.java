package com.cyf.demo.demo.controller;

import com.cyf.demo.demo.service.TestService;
import com.cyf.demo.demo.service.TestServiceDemo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * @author cyf
 * @Time 2023-07-21 01:14
 * @Description
 */
@Controller
public class TestController {
	@Autowired
	private TestService testService;
	@Autowired
	private TestServiceDemo demo;
	public void tse(){
		System.out.println(demo);
		System.out.println(testService);
	}
}
