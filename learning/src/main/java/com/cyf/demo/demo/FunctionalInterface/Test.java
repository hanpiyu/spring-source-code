package com.cyf.demo.demo.FunctionalInterface;

/**
 * @author cyf
 * @Time 2023-07-30 19:08
 * @Description
 */
public class Test {
	public static void main(String[] args) throws Exception {
		test( () -> {
			System.out.println("匿名内部类被调用了");
			return null;
		});
	}
	public static void test(FunctionTesting f) throws Exception {
		System.out.println("test - start");
		f.functionTest();
		System.out.println("test - end");

	}
}
