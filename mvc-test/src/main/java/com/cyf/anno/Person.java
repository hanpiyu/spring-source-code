package com.cyf.anno;


import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.stereotype.Component;

/**
 * @author cyf
 * @Time 2023-05-28 23:18
 * @Description
 */
@Component
public class Person {
	public Person() {
	}

	@PostConstruct
	public void init(){
		System.out.println("初始化方法被调用");
	}

	@PreDestroy
	public void destroy(){
		System.out.println("初始化方法被调用");
	}

}
