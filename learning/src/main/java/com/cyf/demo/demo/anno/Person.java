package com.cyf.demo.demo.anno;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @author cyf
 * @Time 2023-05-28 23:18
 * @Description
 */
@Component
public class Person  implements InstantiationAwareBeanPostProcessor {
	private String name;

	@Override
	public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
		System.out.println("实例化前"+this.getClass());
		return InstantiationAwareBeanPostProcessor.super.postProcessBeforeInstantiation(beanClass, beanName);
	}

	@Override
	public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
		System.out.println("实例化后"+this.getClass());
		return InstantiationAwareBeanPostProcessor.super.postProcessAfterInstantiation(bean, beanName);
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("初始化前" + getClass());
		return InstantiationAwareBeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
	}

	public String name() {
		return name;
	}

	public Person setName(String name) {
		this.name = name;
		Integer a = 1;
		return this;
	}

	public Person() {
	}

	@PostConstruct
	public void init(){
		System.out.println("初始化方法被调用");
	}

	@PreDestroy
	public void destroy(){
		System.out.println("初始化方法被调用");
	}

	@Override
	public String toString() {
		return "Person{" +
				"name='" + name + '\'' +
				'}';
	}
}
