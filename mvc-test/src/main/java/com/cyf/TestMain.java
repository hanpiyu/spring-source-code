package com.cyf;

import com.cyf.bean.MyCalculator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestMain {
	public static void main(String[] args) throws Exception {
//
		ApplicationContext ac = new ClassPathXmlApplicationContext("aaa.xml");
//		GenericApplicationContext genericApplicationContext = new GenericApplicationContext();
		Object person = ac.getBean("person");
		System.out.println();
	}

	public static void main1(String[] args) throws NoSuchMethodException {
		ApplicationContext ac = new ClassPathXmlApplicationContext("aaa.xml");
		MyCalculator bean = ac.getBean(MyCalculator.class);
		bean.add(1, 1);
		System.out.println();
	}
	public static void mai1n(String[] args) throws NoSuchMethodException {
		ApplicationContext ac = new AnnotationConfigApplicationContext();
		MyCalculator bean = ac.getBean(MyCalculator.class);
		bean.add(1, 1);
		System.out.println();
	}
}