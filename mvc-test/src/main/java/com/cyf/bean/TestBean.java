package com.cyf.bean;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 * @author cyf
 * @Time 2023-05-22 07:01
 * @Description
 */
@Component
@ComponentScan("com.cyf.demo.demo.anno")
public class TestBean {
}
