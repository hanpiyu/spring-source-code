package com.cyf.bean;

/**
 * @author cyf
 * @Time 2023-06-06 21:19
 * @Description
 */
public class User {

	private String name;

	private Integer age;




	public User() {
	}
	public User(String name) {
		this.name = name;
	}

	public User(String name, Integer age) {
		this.name = name;
		this.age = age;
	}

	public Integer age() {
		return age;
	}

	public User setAge(Integer age) {
		this.age = age;
		return this;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
