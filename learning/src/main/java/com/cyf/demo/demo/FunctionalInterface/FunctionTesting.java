package com.cyf.demo.demo.FunctionalInterface;

import org.springframework.beans.BeansException;

/**
 * @author cyf
 * @Time 2023-07-30 19:06
 * @Description
 */
@FunctionalInterface
public interface FunctionTesting<T> {
	T functionTest() throws Exception;
}