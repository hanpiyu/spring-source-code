package com.cyf.bean;

/**
 * @author cyf
 * @Time 2023-03-29 01:48
 * @Description
 */

public class Student {
	private String name;

	private Integer age;

	public Student(String name) {

	}

	public Student(String name, Integer age) {
		this.name = name;
		this.age = age;
	}

	public Integer age() {
		return age;
	}

	public Student setAge(Integer age) {
		this.age = age;
		return this;
	}

	public Student() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
