package com.cyf.demo.demo.anno;

import org.springframework.beans.factory.FactoryBean;

/**
 * @author cyf
 * @Time 2023-07-30 18:27
 * @Description
 */
public class FactoryBeanTest implements FactoryBean<Person> {
	@Override
	public Person getObject() throws Exception {
		Person person = new Person();
		person.setName("张三");
		return person;
	}

	@Override
	public Class<?> getObjectType() {
		return null;
	}
}
