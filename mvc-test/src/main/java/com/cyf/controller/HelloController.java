package com.cyf.controller;

import com.cyf.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cyf
 * @Time 2023-06-06 21:19
 * @Description
 */

@Controller
public class HelloController {

	@GetMapping("/userlist")
	public String hello(Model model){
		System.out.println("snials");
		List<User> userList = new ArrayList<>();
		User user1 = new User("张三");
		User user2 = new User("李四");
		userList.add(user1);
		userList.add(user2);
		model.addAttribute("users",userList);
		return "userlist";
	}
}